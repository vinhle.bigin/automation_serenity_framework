package configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestConfigs {
	
	public  WebDriver mywebdriver;
	public String ARCPortal_env ;
	public String TransfereePortal_env;
	public String VendorPortal_env;
	public String ClientPortal_env;
	
	private static String SelectEnv_file = "Properties\\Env_Config.properties";
	private static String QA_env_file = "Properties\\QA_env.properties";
	private static String Uat_env_file = "Properties\\UAT_env.properties";
	private static String Stag_env_file = "Properties\\STAG_env.properties";
	private static String Prod_env_file = "Properties\\PROD.properties";
	private static String SelectBrowser_Prop = "Properties\\BrowserConfig.properties";
	
	private boolean IsFF_blen;
	private boolean IsChr_blen;
	private boolean IsMSEd_blen;
	private boolean IsIE11_blen;
	private boolean IsSafari_blen;
	private String Win_FFdriver_path;
	private String Win_Chrdriver_path ;
	private String Mac_FFdriver_path;
	private String Mac_Chrdriver_path ;
	private boolean IsQA_blen;
	private boolean IsUAT_blen;
	private boolean IsSTAG_blen;
	private boolean IsPROD_blen;
	
	public TestConfigs() {
				
		//1. ===================Read Env Properties Files
		ProptiesFile_manage Select_env_property = new ProptiesFile_manage();
		Select_env_property.LoadPropertyFile(SelectEnv_file);
		
		//Check what env is selected
		IsQA_blen = Boolean.parseBoolean(Select_env_property.property_var.getProperty("isQA"));
		IsUAT_blen = Boolean.parseBoolean(Select_env_property.property_var.getProperty("isUAT"));
		IsSTAG_blen = Boolean.parseBoolean(Select_env_property.property_var.getProperty("isSTAG"));
		IsPROD_blen = Boolean.parseBoolean(Select_env_property.property_var.getProperty("isPROD"));
		
		//2.===================Read Browsers Properties Files
		ProptiesFile_manage OS_properties = new ProptiesFile_manage();
		OS_properties.LoadPropertyFile(SelectBrowser_Prop);
		
		System.out.println("Browser Properties File:"+OS_properties.property_var.toString());
			
		IsFF_blen = Boolean.parseBoolean(OS_properties.property_var.getProperty("FF"));
		IsChr_blen = Boolean.parseBoolean(OS_properties.property_var.getProperty("Chrome"));
		IsSafari_blen = Boolean.parseBoolean(OS_properties.property_var.getProperty("Safari"));
		IsMSEd_blen = Boolean.parseBoolean(OS_properties.property_var.getProperty("MS_Edge"));
		IsIE11_blen = Boolean.parseBoolean(OS_properties.property_var.getProperty("IE_11"));
		
		//Get driver file path
		Win_FFdriver_path = OS_properties.property_var.getProperty("Win_FFdriver_path");
		Win_Chrdriver_path = OS_properties.property_var.getProperty("Win_Chrdriver_path");
		Mac_FFdriver_path = OS_properties.property_var.getProperty("Mac_FFdriver_path");
		Mac_Chrdriver_path = OS_properties.property_var.getProperty("Mac_Chrdriver_path");
		
		
	}//init
	
	
	public void Init_Config()
	{
		InitEnv();
		InitWebDriver();
		
	}
	
	private void InitWebDriver()
	{
		if(IsFF_blen==true)
		{
			InitFF();
		}
		else if(IsChr_blen==true)
		{
			InitChrome();
		}
	}
	
	public void CloseDriver()
	{
		mywebdriver.close();
	}
	
	
	//========================nternal method
	private void InitEnv()
	{
		//Get URLs of the portals
		ProptiesFile_manage env_detal_prop = new ProptiesFile_manage();
		if(IsQA_blen==true)
		{
			env_detal_prop.LoadPropertyFile(QA_env_file);
		}
		else if(IsUAT_blen==true)
		{
			env_detal_prop.LoadPropertyFile(Uat_env_file);
		}
		else if(IsSTAG_blen==true)
		{
			env_detal_prop.LoadPropertyFile(Stag_env_file);
		}
		else if(IsPROD_blen==true)
		{
			env_detal_prop.LoadPropertyFile(Prod_env_file);
		}
				
		ARCPortal_env = env_detal_prop.property_var.getProperty("ARCPortal_env");
		TransfereePortal_env = env_detal_prop.property_var.getProperty("TransfereePortal_env");
		VendorPortal_env = env_detal_prop.property_var.getProperty("VendorPortal_env");
		ClientPortal_env = env_detal_prop.property_var.getProperty("ClientPortal_env");
	}
	
	private void InitFF()
	{
		System.setProperty("webdriver.gecko.driver", Win_FFdriver_path);
		mywebdriver = new FirefoxDriver();
		
	}
	
	private void InitChrome()
	{
		System.setProperty("webdriver.chrome.driver", Win_Chrdriver_path);
		mywebdriver = new ChromeDriver();
		
	}
	
}
