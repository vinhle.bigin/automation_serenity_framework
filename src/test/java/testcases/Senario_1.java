package testcases;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import configuration.TestConfigs;
import configuration.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import objects.Persons;

public class Senario_1 {
	
	@Steps
	Persons	 user_1;
	TestConfigs config;
	
	public Senario_1()
	
	{
		user_1 = new Persons();
		config = new TestConfigs();
		config.Init_Config();
	}
	
	@Given("I want to write a step with precondition")
	public void i_want_to_write_a_step_with_precondition() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
		//driver_manage = new WebDriver_Manage();
		//System.setProperty("webdriver.gecko.driver", "src/test/resources/BrowserDrivers/geckodriver.exe");
		//driver_manage.mywebdriver = new FirefoxDriver();
		
		System.out.println(user_1.Name);
		user_1.Phone = "0903246146";
		
	}

	@Given("some other precondition")
	public void some_other_precondition() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
		System.out.println(user_1.Phone);
	}

	@When("I complete action")
	public void i_complete_action() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
		//Env_Manage myenv = new Env_Manage();

		config.mywebdriver.navigate().to("https://arc-uat.bigin.top");
	}

	@When("some other action")
	public void some_other_action() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}

	@When("yet another action")
	public void yet_another_action() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}

	@Then("I validate the outcomes")
	public void i_validate_the_outcomes() {
	    // Write code here that turns the phrase above into concrete actions
	  // Assert.assertTrue();
	   org.junit.Assert.assertTrue("Test Case is failed due to Phone:"+ user_1.Phone +"doesn't belong to Name:"+user_1.Name, false);
	}

	@Then("check more outcomes")
	public void check_more_outcomes() {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}
	

}
