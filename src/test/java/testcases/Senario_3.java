package testcases;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Senario_3 {

	@Given("i run precondition {int}")
	public void i_run_precondition(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}

	@When("I run action {int}")
	public void i_run_action(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}

	@When("Dont run action {int}")
	public void dont_run_action(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}

	@Then("I validate the result to be Failed")
	public void i_validate_the_result_to_be_Failed() {
		Assert.assertTrue(true);
	    // Write code here that turns the phrase above into concrete actions
	  //  throw new cucumber.api.PendingException();
	}
}
