package testcases;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Senario_4 {

	@Given("I want the test case{int} to be Failed as expected")
	public void i_want_the_test_case_to_be_Failed_as_expected(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	//    throw new cucumber.api.PendingException();
	}

	@When("I run a failed step of the test case")
	public void i_run_a_failed_step_of_the_test_case() {
	    // Write code here that turns the phrase above into concrete actions
	//    throw new cucumber.api.PendingException();
	}

	@When("Input some incorrect data")
	public void input_some_incorrect_data() {
	    // Write code here that turns the phrase above into concrete actions
	//    throw new cucumber.api.PendingException();
	}

	@Then("I see the result as Failed")
	public void i_see_the_result_as_Failed() {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue("This is is Passed instead of FAILED as expected",false);
	  //  throw new cucumber.api.PendingException();
	}

}
